<%@ page import="com.rs.seguridad.Usuario" %>
<g:set var="usuario" value="${Usuario.findByUsername(sec.loggedInUserInfo(field:'username'))}"/>

<div class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Catálogos</a>
            <ul class="dropdown-menu">
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">RS</a>
                    <ul class="dropdown-menu">
                        <li class="dropdown-submenu">
                            <a href="#">Generales</a>
                            <ul class="dropdown-menu">
                                <li><g:link controller="rsGralAsentamiento">Asentamiento</g:link></li>
                                <li><g:link controller="rsGralDelegacionMunicipio">Delegaci&oacute;n y Municipio</g:link></li>
                                <li><g:link controller="rsGralDomicilio">Domicilio</g:link></li>
                                <li><g:link controller="rsGralEstado">Estado</g:link></li>
                                <li><g:link controller="rsGralPais">Pa&iacute;s</g:link></li>
                                <li><g:link controller="rsGralTelefono">Tel&eacute;fono</g:link></li>
                                <li><g:link controller="rsPersona">Persona</g:link></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                        <li class="dropdown-submenu">
                            <a href="#">Cat&aacute;logo</a>
                            <ul class="dropdown-menu">
                                <li><g:link controller="rsCatEmpresa">Empresa</g:link></li>
                                <li><g:link controller="rsCatGrupoEmpresa">Grupo de la empresa</g:link></li>
                                <li><g:link controller="rsCatTipoAsentamiento">Tipo de asentamiento</g:link></li>
                                <li><g:link controller="rsCatTipoPersona">Tipo de persona</g:link></li>
                                <li><g:link controller="rsCatTipoTelefono">Tipo de tel&eacute;fono</g:link></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mesa de Control</a>
            <ul class="dropdown-menu">
                <li><a href="#">Enalce 1</a></li>
                <li><a href="#">Enalce 2</a></li>
                <li><a href="#">Enalce 3</a></li>
                <li><a href="#">Enalce 4</a></li>
                <li><a href="#">Enalce 5</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pieza</a>
            <ul class="dropdown-menu">
                <li><a href="#">Enalce 1</a></li>
                <li><a href="#">Enalce 2</a></li>
                <li><a href="#">Enalce 3</a></li>
                <li><a href="#">Enalce 4</a></li>
                <li><a href="#">Enalce 5</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sucursal</a>
            <ul class="dropdown-menu">
                <li><a href="#">Enalce 1</a></li>
                <li><a href="#">Enalce 2</a></li>
                <li><a href="#">Enalce 3</a></li>
                <li><a href="#">Enalce 4</a></li>
                <li><a href="#">Enalce 5</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Seguridad</a>
            <ul class="dropdown-menu">
                <li><g:link controller="user" action="search" >Usuarios</g:link></li>
                <li><g:link controller="role" action="search" >Roles</g:link></li>
                <li><g:link controller="requestmap" action="search" >URL's</g:link></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manuales</a>
            <ul class="dropdown-menu">
                <li><a href="#">Enalce 1</a></li>
                <li><a href="#">Enalce 2</a></li>
                <li><a href="#">Enalce 3</a></li>
                <li><a href="#">Enalce 4</a></li>
                <li><a href="#">Enalce 5</a></li>
            </ul>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <sec:ifNotLoggedIn>
            <li>
                <g:link controller='login' action='auth'><span class="glyphicon glyphicon-user"> Entrar</span></g:link>
            </li>
        </sec:ifNotLoggedIn>
        <sec:ifLoggedIn>
            <li class="active">
                <a class="dropdown-toggle" role="button" data-toggle="dropdown" data-target="#" href="#"><span class="glyphicon glyphicon-user"> ${usuario?.username}</span></a>
                <ul class="dropdown-menu" role="menu">
                    <li class="">
                        <g:link controller="logout">
                            <span class="glyphicon glyphicon-off"> <g:message code="security.signoff.label" default="Salir"/></span>
                        </g:link>
                    </li>
                </ul>
            </li>
        </sec:ifLoggedIn>

        <g:render template="/_menu/reloj"/>
    <%--
    <g:render template="/barraNavegacion/buscador"/>
    --%>
    </ul>
</div>
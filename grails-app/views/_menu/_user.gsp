<%@ page import="com.seguridad.Usuario" %>
<g:set var="usuario" value="${Usuario.findByUsername(sec.loggedInUserInfo(field:'username'))}"/>
<li class="dropdown">
    <sec:ifNotLoggedIn>
        <g:link controller="login">
            <i class="glyphicon glyphicon-user" class="dropdown-toggle" data-toggle="dropdown"></i>
            <g:message code="security.signin.label"/>
        </g:link>
    </sec:ifNotLoggedIn>

    <sec:ifLoggedIn>
        <a class="dropdown-toggle" role="button" data-toggle="dropdown" data-target="#" href="#">
            <i class="glyphicon glyphicon-user icon-white"></i>
            ${usuario?.username} <b class="caret"></b>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li class="">
                <g:link controller="logout">
                    <i class="glyphicon glyphicon-off"></i>
                    <g:message code="security.signoff.label"/>
                </g:link>
            </li>
        </ul>
    </sec:ifLoggedIn>
</li>